# Markdown2PDF

This is a very basic repository/jupyter-notebook to demonstrate how to convert markdown-files to pdf-files.

Run notebook at [![MyBinder](https://mybinder.org/badge_logo.svg)](https://hub.gke2.mybinder.org/user/ralf.klammer-markdown2pdf-jgo02wwo/doc/workspaces/auto-r/tree/ConvertMD2PDF.ipynb)

## Links

- [https://mybinder.org](https://mybinder.org)
- [Make a repo "binder-ready"](https://the-turing-way.netlify.app/communication/binder/zero-to-binder.html)
- [mdpdf@pypi](https://pypi.org/project/mdpdf/)
